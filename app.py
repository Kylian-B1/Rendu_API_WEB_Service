import os
import requests
import web
from dotenv import load_dotenv
import json

# Generating routes
urls = (
    '/(.*)', 'weather',
)
app = web.application(urls, globals())

load_dotenv()
API_TOKEN = os.getenv('API_TOKEN')

K = 273.15

class weather:
    def GET(self, city):
        web.header('Content-Type', 'application/json')  # text/xml
        r = requests.get('http://api.openweathermap.org/data/2.5/weather?zip={postal_code},fr&appid={}'.format(API_TOKEN, postal_code=city))
        data = r.json()

        result = {
            'actual_temp': data.get('main').get('temp') - K,
            'max_temp': data.get('main').get('temp_max') - K,
            'min_temp': data.get('main').get('temp_min') - K,
            'Cloud': data.get('weather')[0].get('main'),
        }
        return json.dumps(result)

if __name__ == "__main__":
    app.run()
