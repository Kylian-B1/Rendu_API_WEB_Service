## Sujet

Implémentation d'un web service qui retourne la météo prévisionnelle française à partir d'un code postal.
Ce code postal français doit être communiqué en passation de paramètre d'url.

Et retourner uniquement les informations suivantes :

- La température actuelle
- La température min et max de la journée
- La météo

Baser sur l'api de [openweathermap.org](https://home.openweathermap.org).
